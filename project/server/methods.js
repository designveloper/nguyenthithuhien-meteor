import { Meteor} from 'meteor/meteor';
import { Interns } from '../imports/api/Interns';

Meteor.methods ({
  insertIntern(intern){
    Interns.insert(intern);
  }
});
