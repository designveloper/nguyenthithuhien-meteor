import { Meteor } from 'meteor/meteor';
import { Interns } from '../imports/api/Interns';

Meteor.startup(() => {
  Meteor.publish('interns', function(){
    return Interns.find({});
  })
});
